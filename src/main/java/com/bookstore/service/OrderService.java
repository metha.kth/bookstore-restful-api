package com.bookstore.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bookstore.common.ErrorCode;
import com.bookstore.entity.BookEntity;
import com.bookstore.entity.OrderDetailEntity;
import com.bookstore.entity.OrderEntity;
import com.bookstore.entity.UserEntity;
import com.bookstore.exception.BookstoreException;
import com.bookstore.repository.BookRepository;
import com.bookstore.repository.OrderRepository;
import com.bookstore.repository.UserRepository;
import com.bookstore.request.UserOrderBookRequest;
import com.bookstore.response.UserOrderBookResponse;

@Service
@Transactional
public class OrderService{

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BookRepository bookRepository;
	
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	AuthService authService;

	public UserOrderBookResponse save(UserOrderBookRequest request){
		String userName = authService.getTokenData().getUserName();
		Optional<UserEntity> optUser = userRepository.findByUsername(userName);
		if(optUser.isPresent()) {
			List<BookEntity> bookList = bookRepository.findAllById(request.getOrders());
			if(bookList.size() != 0) {
				List<OrderDetailEntity> orderDetailList = new ArrayList<>();
				BigDecimal price = BigDecimal.ZERO;
				for(BookEntity book : bookList) {
					price = price.add(book.getPrice());
					OrderDetailEntity orderDetail = new OrderDetailEntity();
					orderDetail.setBookId(book.getId());
					orderDetailList.add(orderDetail);
				}
				OrderEntity order = new OrderEntity();
				order.setUserId(optUser.get().getId());
				order.setPrice(price);
				order.setOrderDetailList(orderDetailList);
				order = orderRepository.save(order);
				
				UserOrderBookResponse response = new UserOrderBookResponse();
				response.setPrice(order.getPrice());
				return response;
			}
		} else {
			throw new BookstoreException("User not found.", ErrorCode.NO_DATA_FOUND);
		}
		return null;
	}


}