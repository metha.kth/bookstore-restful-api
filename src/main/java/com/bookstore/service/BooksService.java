package com.bookstore.service;

import com.bookstore.entity.BookEntity;
import com.bookstore.repository.BookRepository;
import com.bookstore.response.BooksDataResponse;
import com.bookstore.response.BooksFormPublisherResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class BooksService {

	@Autowired
	BookRepository bookRepository;

	@Autowired
	PublisherService publisherService;

	public List<BooksDataResponse> getBooks() {

		List<BooksDataResponse> dataList = new ArrayList<>();
		BooksFormPublisherResponse[] recommendList = publisherService.callGetListRecommendedFromPublisher();
		List<Long> recommendIdList = new ArrayList<>();
		for (BooksFormPublisherResponse book : recommendList) {
			BooksDataResponse data = new BooksDataResponse();
			recommendIdList.add(book.getId());
			data.setId(book.getId());
			data.setName(book.getBook_name());
			data.setAuthor(book.getAuthor_name());
			data.setPrice(book.getPrice());
			data.setIs_recommended(true);
			dataList.add(data);
		}

		List<BookEntity> bookList = bookRepository.findByIdNotIn(recommendIdList);

		for (BookEntity book : bookList) {
			BooksDataResponse data = new BooksDataResponse();
			data.setId(book.getId());
			data.setName(book.getBookName());
			data.setAuthor(book.getAuthorName());
			data.setPrice(book.getPrice());
			data.setIs_recommended(false);
			dataList.add(data);
		}

		return dataList;
	}

	public List<BookEntity> updateBookList(List<BookEntity> bookEntityList) {
		List<BookEntity> bookList = bookRepository.saveAll(bookEntityList);
		return bookList;
	}
}
