package com.bookstore.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.bookstore.entity.UserEntity;
import com.bookstore.repository.UserRepository;

@Service(value = "userDetailsService")
public class CustomUserDetailService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<UserEntity> optUser = userRepository.findByUsername(username);
		if(!optUser.isPresent()){
			throw new UsernameNotFoundException("User not found : " + username);
		}
		List<GrantedAuthority> grantedAuthorityList = new ArrayList<>();
		GrantedAuthority authority = new SimpleGrantedAuthority("USER");
		grantedAuthorityList.add(authority);
		return new org.springframework.security.core.userdetails.User(optUser.get().getUsername(), optUser.get().getPassword(), grantedAuthorityList);
	}
	
}
