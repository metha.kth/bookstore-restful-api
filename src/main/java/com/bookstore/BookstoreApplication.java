package com.bookstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.bookstore.jobschedule.BooksSchedule;

@SpringBootApplication
@EnableScheduling
public class BookstoreApplication {

	public static void main(String[] args) {
		ApplicationContext applicationContext = SpringApplication.run(BookstoreApplication.class, args);
		
		// Load list of book when startup server.
		BooksSchedule booksSchedule = applicationContext.getBean(BooksSchedule.class);
		booksSchedule.loadBooksFromPublisher();;
	}

}


