package com.bookstore.repository;

import com.bookstore.entity.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<BookEntity, Long> {

	public List<BookEntity> findByIdNotIn(List<Long> id);

}
