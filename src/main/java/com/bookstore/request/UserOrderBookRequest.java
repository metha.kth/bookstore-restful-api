package com.bookstore.request;

import java.util.List;

import lombok.Data;

@Data
public class UserOrderBookRequest {
	private List<Long> orders;
}
