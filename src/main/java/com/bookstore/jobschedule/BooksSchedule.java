package com.bookstore.jobschedule;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.bookstore.common.ErrorCode;
import com.bookstore.entity.BookEntity;
import com.bookstore.exception.BookstoreException;
import com.bookstore.response.BooksFormPublisherResponse;
import com.bookstore.service.BooksService;
import com.bookstore.service.PublisherService;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class BooksSchedule {

	@Autowired
	BooksService booksService;
	
	@Autowired
	PublisherService publisherService;

	// At 00:15:00am, on every Sunday, every month
	@Scheduled(cron = "0 15 0 ? * SUN")
	public void loadBooksFromPublisher(){
		
		BooksFormPublisherResponse[] data = publisherService.callGetListBooksFromPublisher();
		
		List<BookEntity> listBookEntity = new ArrayList<>();
		for(BooksFormPublisherResponse book: data){
			BookEntity bookEntity = new BookEntity();
			bookEntity.setId(book.getId());
			bookEntity.setBookName(book.getBook_name());
			bookEntity.setPrice(book.getPrice());
			bookEntity.setAuthorName(book.getAuthor_name());
			listBookEntity.add(bookEntity);
		}

		List<BookEntity> listUpdateBook = booksService.updateBookList(listBookEntity);

		if(listUpdateBook.equals(listBookEntity)){
			log.info("Update list of book success");
		} else {
			log.error("Update list of book fail");
			throw new BookstoreException("Update list of book fail", ErrorCode.UPDATE_BOOK_FAIL);
		}
	}

}
