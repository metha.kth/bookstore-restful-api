package com.bookstore.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "user_bookstore")
@Data
public class UserEntity {

	@Id
	@SequenceGenerator(name="user_bookstore_id_seq",sequenceName="user_bookstore_id_seq", allocationSize = 1)
	@GeneratedValue(strategy= GenerationType.SEQUENCE, generator="user_bookstore_id_seq")
	@Column(name="id")
	private Long id;

	@Column(name="username")
	private String username;

	@Column(name="password")
	private String password;

	@Column(name="date_of_birth")
	private Date dateOfBirth;
	
	@Column(name="name")
	private String name;
	
	@Column(name="surname")
	private String surname;

}
